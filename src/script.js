class WebEventsRecorder {
	constructor() {
		this.bindFunctions();
		this.initProps();
		this.bindEvents();
		this.startPostDataTimer();
	}

	initProps() {
		this.events = [];
		this.endpoint = 'http://localhost:3000/api/data';
		this.sendDataInterval = 1000;
	}

	bindFunctions() {
		this.registerEvent = this.registerEvent.bind(this);
		this.sendDataAndFlushEvents = this.sendDataAndFlushEvents.bind(this);
	}

	bindEvents() {
		window.addEventListener('load', this.registerEvent);
		window.addEventListener('mousemove', this.registerEvent);
		window.addEventListener('click', this.registerEvent);
		window.addEventListener('scroll', this.registerEvent);
		window.addEventListener('keydown', this.registerEvent);
		window.addEventListener('keyup', this.registerEvent);
		window.addEventListener('unload', this.registerEvent);
	}

	registerEvent(rawEvent) {
		const flatEventData = WebEventsRecorder.getFlatEventData(rawEvent);
		const event = WebEventsRecorder.createEvent(flatEventData);
		this.events.push(event);
	}

	startPostDataTimer() {
		setInterval(this.sendDataAndFlushEvents, this.sendDataInterval);
	}

	sendDataAndFlushEvents() {
		const requestPayload = {
			key: "events",
			value: [...this.events]
		};
		this.events = [];

		return fetch(this.endpoint, {
			method: "POST",
			headers: {
				'Accept': 'application/json, text/plain, */*',
				'Content-Type': 'application/json'
			},
			body: JSON.stringify(requestPayload)
		})
			.then(res => res.json())
			.then(() => console.log('sendPageData success'))
			.catch(() => console.log('sendPageData error'));
	}

	static createEvent(flatEventData) {
		return {
			timestamp: Date.now(),
			url: window.location.href,
			eventData: flatEventData,
		};
	}

	static getFlatEventData(event) {
		const flatEvent = {};
		for (const key in event) {
			const value = event[key];
			const shouldSet =
				!Array.isArray(value)
				&& typeof value !== 'object'
				&& typeof value !== 'function';

			if (shouldSet) {
				flatEvent[key] = value;
			}
		}
		return flatEvent;
	}
}

new WebEventsRecorder();
